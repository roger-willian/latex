# LaTeX docker image
TeXLive + Inkscape + gnuplot + GNU Make image for typesetting scientific documents

## Base Image
Newer versions (prod-0.2.x) of this image are based on debian:stable-slim. 
Previous versions (prod-0.1.x) were based on ubuntu 20.04.
The final image also includes some utilities described in the next sections.

## TeXLive
For typesetting a scientific document out of a LaTeX file like:
```
$ pdflatex root.tex
$ bibtex root
$ pdflatex root.tex
$ pdflatex root.tex
```
It is not texlive-full, it includes only the following packages:
 - texlive-latex-extra
 - texlive-fonts-extra
 - texlive-lang-portuguese (because sometimes I write in portuguese)
 - texlive-publishers
 - texlive-science

## latexmk
So instead of the previous four commands you only have to use:
```
$ latexmk -pdf root.tex
```

## Gnuplot
So you can generate high-quality plots using commands like:
```
$ gnuplot -c myplot.plt
$ pdflatex myplot.tex
```
if myplot.plt uses the cairolatex driver, for example.

## Inkscape
So you can generate high-quality line art figures using commands like:
```
$ inkscape --export-area-page --export-latex --export-pdf mydrawing.svg
$ pdflatex mydrawing.pdf_tex
```

## GNU Make
So you can automate the whole process using a makefile and commands like:
```
$ make article
$ make presentation
```

## libxml2-utils
This is only there in case you need to manipulate the SVG files in a script, to extract some metadata, for example:
```
$ xmllint --xpath '//*[name()="dc:creator"]//*[name()="dc:title"]/text()'
```