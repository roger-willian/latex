FROM debian:stable-slim
RUN apt-get -y update && apt-get install -y \
  texlive-latex-extra \
  texlive-fonts-extra \
  texlive-publishers \
  texlive-science
RUN apt-get install -y texlive-lang-portuguese
RUN apt-get install -y latexmk \
  make \
  gnuplot \
  inkscape \
  libxml2-utils
